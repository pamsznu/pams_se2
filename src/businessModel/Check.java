package BusinessModel;

import java.util.Date;

public abstract class Check {

	/*@ spec_public @*/ Profile  profile;
	/*@ spec_public @*/ Person  person;
	protected /*@ spec_public @*/ int Id;
	protected /*@ spec_public @*/ long amount;
	protected /*@ spec_public @*/ Date dueDate;
	protected /*@ spec_public @*/ String serial;
	protected /*@ spec_public @*/ String comment;
	protected /*@ spec_public @*/ boolean status;

	//@ public invariant this.Id >=0 && this.amount >= 0;
	
	//@ requires this.Id>=0;
	//@ ensures \result == this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@ requires Id>=0;
	//@ assignable this.Id;
	//@ ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}
	//@requires this.amount >= 0;
	//@ ensures \result == this.amount;
	public /*@ pure @*/ long getAmount() {
		return this.amount;
	}

	//@requires amount >= 0;
	//@ assignable this.amount;
	//@ ensures this.amount == amount;
	public void setAmount(long amount) {
		this.amount = amount;
	}

	public /*@ pure @*/ Date getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	//@requires this.serial != null;
	//@ ensures \result == this.serial;
	public /*@ pure @*/ String getSerial() {
		return this.serial;
	}

	//@requires serial != null;
	//@assignable this.serial;
	//@ensures this.serial == serial;
	public void setSerial(String serial) {
		this.serial = serial;
	}

	//@ ensures \result == this.comment;
	public /*@ pure @*/ String getComment() {
		return this.comment;
	}

	//@requires comment != null;
	//@ assignable this.comment;
	//@ ensures this.comment == comment;
	public void setComment(String comment) {
		this.comment = comment;
	}

	//@ensures \result == this.status;
	public /*@ pure @*/ boolean isStatus() {
		return this.status;
	}

	//@ assignable this.status;
	//@ ensures this.status == status;
	public void setStatus(boolean status) {
		this.status = status;
	}

	//@requires amount >=0 && serial != null && comment != null;
	//@assignable this.amount && this.dueDate && this.serial && this.commetn && this.status && this.person && this.profile;
	//@ensures this.amount == amount && this.dueDate == date;
	//@ensures this.serial == serial && this.comment == comment;
	//@ensures this.status == status && this.person== person && this.profile == profile;
	//@ensures \result == true;
	public abstract boolean Add(long amount, Date date, String serial, String comment, boolean status,/*@ non_null @*/ Person person,/*@ non_null @*/ Profile profile);

	//@requires Id >= 0 && amount >=0 && serial != null && comment != null;
	//@assignable this.amount && this.dueDate && this.serial && this.commetn && this.status && this.person && this.Id;
	//@ensures this.Id == Id && this.amount == amount && this.dueDate == date;
	//@ensures this.serial == serial && this.comment == comment;
	//@ensures this.status == status && this.person== person ;
	//@ensures \result == true;
	public abstract /*@ pure @*/ boolean Update(int Id, long amount, Date date, String serial, String comment, boolean status,/*@ non_null @*/ Person person);

	//@requires Id >=0 ;
	//ensures \result == true;
	public abstract /*@ pure @*/ boolean Delete(int Id);

	
	public /*@ pure @*/ boolean ChangeStatus(boolean status) {
		throw new UnsupportedOperationException();
	}

}