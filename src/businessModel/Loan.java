package BusinessModel;

import java.util.*;

/**
 * وام های هر کاربر 
 */
public class Loan {

	/*@ spec_public @*/ Profile profile;
	/*@ spec_public @*/ TLoan tLoan;
	/*@ spec_public @*/ ArrayList<RefundLoan> refundloan;
	/**
	 * شناسه هر وام
	 * توسط سیستم تولید می شود
	 */
	private /*@ spec_public @*/ int Id;
	/**
	 * نام وام 
	 */
	private /*@ spec_public @*/ String name;
	/**
	 * مبلغ هر وام
	 */
	private /*@ spec_public @*/ long amount;
	private /*@ spec_public @*/ String comment;
	/**
	 * تاریخ شروع وام
	 */
	private /*@ spec_public @*/ Date startDate;
	/**
	 * تعداد اقساط وام 
	 */
	private /*@ spec_public @*/ int numberInstallment;
	private /*@ spec_public @*/ boolean status;
	private /*@ spec_public @*/ int numberPayUp;
	
	//@ public invariant Id>=0 && numberInstallment>=0 && numberPayUp>=0 ;

	//@ensures \result == this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@requires Id >= 0;
	//@assignable this.Id;
	//@ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@requires \result == this.name;
	public /*@ pure @*/ String getName() {
		return this.name;
	}

	//@requires name != null;
	//@assignable this.name;
	//@ensures this.name == name;
	public void setName(String name) {
		this.name = name;
	}

	//@ensures \result == this.amount;
	public /*@ pure @*/ long getAmount() {
		return this.amount;
	}

	//@requires amount >= 0;
	//@assignable this.amount;
	//@ensures this.amount == amount;
	public void setAmount(long amount) {
		this.amount = amount;
	}

	//@ensures \result == this.comment;
	public /*@ pure @*/ String getComment() {
		return this.comment;
	}

	//@requires comment;
	//@assignable this.comment;
	//@ensures this.comment == comment;
	public void setComment(String comment) {
		this.comment = comment;
	}

	//@ensures \result == this.startDate;
	public /*@ pure @*/ Date getStartDate() {
		return this.startDate;
	}

	//@requires startDate != null;
	//@assignable this.startDate;
	//@ensures this.startDate == startDate;
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	//@ensures \result == this.numberInstallment;
	public /*@ pure @*/ int getNumberInstallment() {
		return this.numberInstallment;
	}

	//@requires numberInstallment >=0;
	//@assignable this.numberInstallment;
	//@ensures this.numberInstallment == numberInstallment;
	public void setNumberInstallment(int numberInstallment) {
		this.numberInstallment = numberInstallment;
	}

	//@ensures \result == this.status;
	public /*@ pure @*/ boolean isStatus() {
		return this.status;
	}

	//@assignable this.status;
	//@ensures this.status == status;
	public void setStatus(boolean status) {
		this.status = status;
	}

	//@ensures \result == this.numberPayUp;
	public /*@ pure @*/ int getNumberPayUp() {
		return this.numberPayUp;
	}

	//@requires numberPayUp >= 0;
	//@assignable this.numberPayUp;
	//@ensures this,.numberPayUp == this.numberPayUp;
	public void setNumberPayUp(int numberPayUp) {
		this.numberPayUp = numberPayUp;
	}

	//@requires name != null && amount >=0 && numberInstallment > =0 && numberPayUp >= 0;
	//@assignable this.name && this.amount && this.comment && this.startDate && this.numberInstallment;
	//@assignable this.status && this.numberPayUp && this.profile;
	//@ensures this.name == name && this.amount == amount && this.comment == comment ;
	//@ensures this.startDate == startDate && this.numberInstallment == numberInstallment;
	//@ensures this.status == status && this.numberPayUp == numberPayUp && this.profile == profile;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Add(String name, long amout, String comment, Date startDate, int numberInstallment, boolean status, int numberPayUp,/*@ non_null @*/ Profile Profile) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >= 0 && name != null && amount >=0 && numberInstallment > =0 && numberPayUp >= 0;
	//@assignable this.name && this.amount && this.comment && this.startDate && this.numberInstallment;
	//@assignable this.status && this.numberPayUp && this.Id;
	//@ensures this.name == name && this.amount == amount && this.comment == comment ;
	//@ensures this.startDate == startDate && this.numberInstallment == numberInstallment;
	//@ensures this.status == status && this.numberPayUp == numberPayUp && this.Id == Id;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Update(int Id, String name, long amout, String comment, Date startDate, int numberInstallment, boolean status, int numberPayUp) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >= 0;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ int Delete(int Id) {
		throw new UnsupportedOperationException();
	}

	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean AddAmount2Income() {
		throw new UnsupportedOperationException();
	}

	//@ensures \result ==true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean GenerateRefundLoan() {
		throw new UnsupportedOperationException();
	}

	//@ensures \result ==true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean ComputeFine() {
		throw new UnsupportedOperationException();
	}

}