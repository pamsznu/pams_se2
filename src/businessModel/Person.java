package BusinessModel;

/**
 * تمام اشخاصی که کاربر با آن ها ارتباطات مالی دارند
 */
public class Person {

	/*@ spec_public @*/ Profile profile;
	/*@ spec_public @*/ Check check;
	/*@ spec_public @*/ FinancialCommitment financialCommitment;
	private /*@ spec_public @*/ int Id;
	private /*@ spec_public @*/ String name;
	private /*@ spec_public @*/ int AccountNumber;
	
	//@public invariant Id >= 0 && AccountNumber >=0 ;


	//@ requires this.Id >= 0;
	//@ ensures \result == this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@ requires Id >= 0;
	//@ assignable this.Id;
	//@ ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@ requires this.name !=null ;
	//@ ensures \result == this.name;
	public /*@ pure @*/ String getName() {
		return this.name;
	}

	//@ requires name != null;
	//@ assignable this.name;
	//@ ensures this.name == name;
	public void setName(String name) {
		this.name = name;
	}

	//@requires this.AccountNumber >=0;
	//@ensures \result == this.AccountNumber;
	public /*@ pure @*/ int getAccountNumber() {
		return this.AccountNumber;
	}

	//@ requires AccountNumber >= 0;
	//@ assignable this.AccountNumber;
	//@ ensures this.AccountNumber == AccountNumber;
	public void setAccountNumber(int AccountNumber) {
		this.AccountNumber = AccountNumber;
	}

	//@ requires accNumber >= 0 ;
	//@assignable this.name && this.accNumber && this.profile;
	//@ensures this.name == name && this.accNumber == accNumber && this.profile == profile;
	//@ ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Add(String name, int accNumber,/*@ non_null@*/ Profile profile) {
		throw new UnsupportedOperationException();
	}

	//@ requires accNumber >= 0 && Id >= 0 ;
	//@assignable this.name && this.accNumber && this.Id;
	//@ensures this.name == name && this.accNumber == accNumber && this.Id == Id;
	//@ ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Update(int Id, String name, int accNumber) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >= 0;
	//@ensures \result == true;
	public /*@ pure @*/ boolean Delete(int Id) {
		throw new UnsupportedOperationException();
	}

}