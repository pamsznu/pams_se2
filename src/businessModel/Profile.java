package BusinessModel;

import java.util.*;

/**
 * همه کاربران برنامه یک پروفایل کاربری در سطح سیستم دارند
 */
public class Profile {

	/*@spec_public@*/ Collection<Account> account;
	private /*@spec_public@*/ int Id;
	/**
	 * نام کاربری برای هر کاربر 
	 * نام کاربری باید یکتا باشد.
	 */
	private /*@spec_public@*/ String username;
	/**
	 * ایمیل کاربر باید منحصربفرد باشد
	 */
	private /*@spec_public@*/ String Email;
	private /*@spec_public@*/ String password;
	/*@spec_public@*/ ArrayList<Loan> loan;
	/*@spec_public@*/ ArrayList<Tag> tag;
	/*@spec_public@*/ ArrayList<Person> person;
	/*@spec_public@*/ ArrayList<Type> type;
	/*@spec_public@*/ ArrayList<FinancialCommitment> financialCommitment;
	/*@spec_public@*/ ArrayList<Check> check;
	
	//@public invariant Id >= 0 && password != null && username != null && password != null;
	//@public invariant tag.size() >=0 && person.size() >=0 && type.size() >=0 && tag.financialCommitment() >=0 ;


	//@requires this.Id >=0;
	//@ensures \result == this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@requires Id>=0;
	//@assignable this.Id;
	//@ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@requires this.username !=null;
	//@ensures \result == this.username;
	public /*@ pure @*/ String getUsername() {
		return this.username;
	}

	//@requires username!= null;
	//@assignable this.username;
	//@ensures this.username == username;
	public void setUsername(String username) {
		this.username = username;
	}

	//@requires this.Email !=null;
	//@ensures \result == this.Email;
	public /*@ pure @*/ String getEmail() {
		return this.Email;
	}
	
	//@requires Email!= null;
	//@assignable this.Email;
	//@ensures this.Email == Email;	
	public void setEmail(String Email) {
		this.Email = Email;
	}

	//@requires this.password != null;
	//@ensures \result == this.password;
	public /*@ pure @*/ String getPassword() {
		return this.password;
	}

	//@requires password!= null;
	//@assignable this.password;
	//@ensures this.password == password;
	public void setPassword(String password) {
		this.password = password;
	}

	//@ensures username != null && password != null && email != null;
	//@ assignable this.username && this.password && this.Email;
	//@ensures this.username == username;
	//@ensures this.password == password;
	//@ensures this.Email == email;
	//@ signals_only UnsupportedOperationException;
	public  Profile(String username, String password, String email) {
		throw new UnsupportedOperationException();
	}

	//@requires password != null;
	//@assignable this.password;
	//@ensures this.password == password;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean updatePassword(String password) {
		throw new UnsupportedOperationException();
	}

	/**
	 * ثبت نام 
	 * @param username
	 * @param password
	 * @return 
	 */
	//@requires username != null && password != null;
	//@assignable this.username && this.password;
	//@ensures this.username == username && this.password == password;
	//@ ensures \result == Profile;
	//@ signals_only UnsupportedOperationException;
	public  static /*@ pure @*/ Profile SignIn(String username, String password) {
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return 
	 */
	//@requires username != null && password != null;
	//@assignable this.username && this.password;
	//@ensures this.username == username && this.password == password;
	//@ ensures \result == Profile;
	//@ signals_only UnsupportedOperationException;
	public static /*@ pure @*/ Profile SignUp(String username, String password) {
		throw new UnsupportedOperationException();
	}

}