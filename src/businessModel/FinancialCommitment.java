package BusinessModel;

import java.util.Date;



public abstract class FinancialCommitment {

	/*@ spec_public @*/Profile profile;
	/*@ spec_public @*/ Person person;
	protected /*@ spec_public @*/ int Id;
	protected /*@ spec_public @*/ long amount;
	protected /*@ spec_public @*/ Date dueDate;
	protected /*@ spec_public @*/ boolean status = false;
	
	//@ public invariant Id >=0 && amount >=0;


	//@requires this.Id >=0;
	//@ ensures \result == this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@requires Id >=0;
	//@ assignable this.Id;
	//@ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@requires this.amount >=0;
	//@ensures \result == this.amount;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ long getAmount() {
		return this.amount;
	}

	//@requires amount >=0;
	//@assignable this.amount;
	//@ensures this.amount == amount;
	public void setAmount(long amount) {
		this.amount = amount;
	}

	//@ensures \result == this.dueDate;
	public /*@ pure @*/ Date getDueDate() {
		return this.dueDate;
	}

	//@requires dueDate != null;
	//@assignable this.dueDate;
	//@ensures this.dueDate == dueDate;
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	//@ensures \result == this.status;
	public /*@ pure @*/ boolean isStatus() {
		return this.status;
	}

	//@assignable this.status;
	//@ensures this.status == status;
	public void setStatus(boolean status) {
		this.status = status;
	}

	//@requires amount >=0 && dueDate != null ;
	//@assignable this.person && this.amount && this.dueDate && this.profile;
	//@ensures this.person == person && this.amount == amount;
	//@ensures this.dueDate == dueDate && this.profile == profile;
	//@ensures \result == true;
	public abstract /*@ pure @*/ boolean Add(/*@ non_null @*/@Person person, long amount, Date dueDate,/*@ non_null @*/ Profile profile);

	//@requires Id >=0 && amount >=0 && dueDate != null ;
	//@assignable this.Id && this.amount && this.dueDate && this.person && this.status;
	//@ensures this.person == person && this.amount == amount;
	//@ensures this.dueDate == dueDate && this.Id == Id && this.status == status;
	//@ensures \result == true;
	public abstract /*@ pure @*/ boolean update(int Id, /*@ non_null @*/Person person, long amount, Date dueDate, boolean status);

	
	//@requires Id >=0;
	//@ensures \result == true;
	public abstract /*@ pure @*/ boolean delete(int Id);

	//@ensures \result == true;
	public abstract /*@ pure @*/ boolean changeStatus(boolean status);

}