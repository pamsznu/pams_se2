package BusinessModel;

import java.util.Date;

/**
 * تراکنش های کاربر از این کلاس ارث بری می کنند
 */
public abstract class Transaction {

	/*@ spec_public @*/ Tag tag;
	/**
	 * شماره ی هر تراکنش 
	 */
	protected /*@ spec_public @*/ int Id;
	/**
	 * مقدار تراکنش
	 */
	protected /*@ spec_public @*/ long amount;
	/**
	 * تاریخ انجام تراکنش 
	 */
	protected /*@ spec_public @*/ Date date;
	/*@ spec_public @*/ Account account;
	
	//@ public invariant Id >=0;

	//@requires this.Id >=0;
	//@ensures \result ==this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@ requires Id>=0;
	//@ assignable this.Id;
	//@ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@requires this.amount >=0;
	//@ensures \result == this.amount;
	public /*@ pure @*/ long getAmount() {
		return this.amount;
	}

	//@requires amount >=0;
	//@assignable this.amount;
	//@ensures this.amount == amount;
	public void setAmount(long amount) {
		this.amount = amount;
	}

	//@ensures \result == this.date;
	public /*@ pure @*/Date getDate() {
		return this.date;
	}

	//@assignable this.date;
	//@ensures this.date == date;
	public void setDate(Date date) {
		this.date = date;
	}

	//@requires amount >=0 && date != null;
	//@assignable this.amount && this.date && this.type && this.account;
	//@ensures this.amount == amount && this.date == date;
	//@ensures this.type == type && this.account == account;
	//@ensures \result == true;
	public abstract /*@ pure @*/ boolean Add(long amount, Date date, /*@ non_null @*/Type type,/*@ non_null @*/ Account account);

	//@requires amount >=0 && date != null && Id > =0;
	//@assignable amount this.amount && this.date && this.type && this.Id;
	//@ensures this.amount == amount && this.date == date;
	//@ensures this.type == type && this.Id == Id;
	//@ensures \result == true;
	public /*@ pure @*/ boolean  Update(long amount, Date date, /*@ non_null @*/Type type, int Id) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >= 0;
	//@ensures \result == true;
	public /*@ pure @*/ int Delete(int Id) {
		throw new UnsupportedOperationException();
	}

}