package BusinessModel;

import java.util.*;


public class Account {

	/*@spec_public@*/Profile  profile;

	private /*@spec_public@*/ int Id;

	private /*@spec_public@*/ String name;
	/*@spec_public@*/ TAccount  tAccount;

	private /*@spec_public@*/ int number;
	
	 private /*@spec_public@*/ long firstBalance;
	/*@spec_public@*/ ArrayList  <Transaction> transaction;
	
	//@public invariant Id >=0 && firstBalance>=0 ;


	//@requires this.Id >= 0;
	//@ ensures \result == this.Id;
	public /*@pure@*/ int getId() {
		return this.Id;
	}

	//@ requires Id >= 0 ;
	//@ assignable this.Id ;
	//@ ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@requires this.name != null;
	//@ ensures \result == this.name;
	public /*@pure@*/ String getName() {
		return this.name;
	}

	//@ requires name!= null ;
	//@ assignable this.name ;
	//@ ensures this.name == name;
	public void setName(String name) {
		this.name = name;
	}

	//@requires this.number >=0;
	//@ ensures \result == this.number;
	public /*@pure@*/ int getNumber() {
		return this.number;
	}

	//@ requires number >=0 ;
	//@ assignable this.number ;
	//@ ensures this.number == number;
	public void setNumber(int number) {
		this.number = number;
	}

	//@requires this.firstBalance >=0;
	//@ensures \result == this.firstBalance;
	public /*@pure@*/ long getFirstBalance() {
		return this.firstBalance;
	}

	//@ requires firstBalance >=0 ;
	//@ assignable this.firstBalance ;
	//@ ensures this.firstBalance == firstBalance;
	public void setFirstBalance(long firstBalance) {
		this.firstBalance = firstBalance;
	}

	
	
	//@requires name!= null && accNumber >=0 && fbalance >=0;
	//@assignable this.name && this.number && this.tAccount && this.firstBalance && this.profile;
	//@ ensures this.name == name;
	//@ ensures this.tAccount == typeAccount;
	//@ ensures this.number == accNumber;
	//@ ensures this.firstBalance == fbalance;
	//@ ensures this.profile == profile;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@pure@*/ boolean Add(String name, int accNumber,/*@ non_null @*/ TAccount typeAccount, long fbalance, /*@ non_null @*/Profile profile) {
		throw new UnsupportedOperationException();
	}

	//@requires Id>=0 && name!= null && accNumber >=0 && fbalance >=0;
	//@assignable this.Id && tihs.name && this.number && this.tAccount && this.firstBalance;
	//@ ensures this.name == name;
	//@ ensures this.tAccount == typeAccount;
	//@ ensures this.number == accNumber;
	//@ ensures this.firstBalance == fbalance;
	//@ ensures this.Id == Id;
	//@ensures \result == true;	
	//@ signals_only UnsupportedOperationException;
	public /*@pure@*/ boolean update(int Id, String name, int accNumber, /*@ non_null @*/TAccount typeAccount, long fBalance) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >=0;
	//@ensures \result ==true;
	//@ signals_only UnsupportedOperationException;
	public /*@pure@*/ boolean Delete(int Id) {
		throw new UnsupportedOperationException();
	}

}