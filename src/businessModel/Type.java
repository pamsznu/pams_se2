package BusinessModel;

/**
 * تمامی انواع نوع ها از این کلاس ارث بری می کنند
 */
public abstract class Type {

	/**
	 * شماره اختصاصی به هر نوع 
	 */
	protected /*@ spec_public @*/ int Id;
	/**
	 * نام هر نوع
	 */
	protected /*@ spec_public @*/ String name;
	/**
	 * برای دسته بندی کردن نوع ها به کار می رود 
	 */
	protected /*@ spec_public @*/ int subset;
	/*@ spec_public @*/ Profile profile;
	
	//@public invariant Id >=0 && subset >= 0;

	//@ensures \result == this.Id;	
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@requires Id>=0;
	//@assignable this.Id;
	//@ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@ensures \result == this.name;
	public /*@ pure @*/ String getName() {
		return this.name;
	}

	//@requires name != null;
	//@assignable this.name;
	//@ensures this.name == name;
	public void setName(String name) {
		this.name = name;
	}

	//@ensures \result == this.subset;	
	public /*@ pure @*/ int getSubset() {
		return this.subset;
	}

	//@requires subset >=0;
	//@assignable this.subset;
	//@ensures this.subset == subset;
	public void setSubset(int subset) {
		this.subset = subset;
	}

	//@requires name != null && subset >= 0;
	//@assignable this.name && this.subset && this.profile;
	//@ensures this.name == name && this.subset == subset && this.profile == profile;
	//@ensures \result ==true;
	public abstract /*@ pure @*/ boolean Add(String name, int subset,/*@ non_null@*/ Profile profile);

	
	//@requires name != null && subset >= 0 && Id >= 0;
	//@assignable this.name && this.subset && this.Id;
	//@ensures this.name == name && this.subset == subset && this.Id == Id;
	//@ensures \result ==true;
	public abstract /*@ pure @*/ boolean update(int id, String name, int subset);

	//@requires Id >= 0;
	//@ensures \result ==true;
	public abstract /*@ pure @*/ boolean Delete(int Id);

}