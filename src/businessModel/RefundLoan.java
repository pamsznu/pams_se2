package BusinessModel;

import java.util.Date;

public class RefundLoan {

	/*@ spec_public @*/ Loan loan;
	private /*@ spec_public @*/ int Id;
	private /*@ spec_public @*/ long amount;
	private /*@ spec_public @*/ Date dueDate;
	private /*@ spec_public @*/ String comment;
	private /*@ spec_public @*/ boolean status;
	
	//@public invariant Id>=0 && amount >= 0;


	//@ensures \result == this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;
	}

	//@requires Id>=0;
	//@ assignable this.Id;
	//@ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}
	
	//@ensures \result == this.amount;
	public /*@ pure @*/ long getAmount() {
		return this.amount;
	}

	//@requires amount >=0;
	//@assignable this.amount;
	//@ensures this.amount == amount;
	public void setAmount(long amount) {
		this.amount = amount;
	}

	//@ensures \result == this.dueDate;
	public /*@ pure @*/ Date getDueDate() {
		return this.dueDate;
	}

	//@requires dueDate != null;
	//@assignable this.dueDate;
	//@ensures this.dueDate == dueDate;
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	//@ensures \result == this.comment;
	public /*@ pure @*/ String getComment() {
		return this.comment;
	}

	//@assignable this.comment;
	//@ensures this.comment == comment;
	public void setComment(String comment) {
		this.comment = comment;
	}

	//@ensures \result == this.status;
	public /*@ pure @*/ boolean isStatus() {
		return this.status;
	}

	//@assignable this.status;
	//@ensures this.status == status;
	public void setStatus(boolean status) {
		this.status = status;
	}

	//@requires amount >=0 &&  date != null;
	//@assignable this.amount && this.dueDate && this.comment && this.status && this.loan;
	//@ensures this.amount == amount && this.dueDate == date && this.status == status && this.loan == loan;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Add(long amount, Date date, String comment, boolean status,/*@ non_null@*/ Loan loan) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >= 0 && amount >=0 &&  date != null;
	//@assignable this.amount && this.dueDate && this.comment && this.status && this.Id;
	//@ensures this.amount == amount && this.dueDate == date && this.status == status && this.Id == Id;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Update(int Id, long amount, Date date, String comment, boolean status) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >= 0;
	//@ensures \result ==true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Delete(int Id) {
		throw new UnsupportedOperationException();
	}

	//@ensures \result ==true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean AddLiquidate2Expense() {
		throw new UnsupportedOperationException();
	}

	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean changeStatus(boolean status) {
		throw new UnsupportedOperationException();
	}

}