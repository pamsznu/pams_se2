package BusinessModel;

import java.util.*;

public class Tag {

	/*@ spec_public @*/ Profile profile;
	/*@ spec_public @*/ TTag tTag;
	/*@ spec_public @*/ ArrayList<Transaction> transaction;
	private /*@ spec_public @*/ int Id;
	
	//@public invariant Id >= 0 && transaction.size() >=0;



	//@ensures \result == this.Id;
	public /*@ pure @*/ int getId() {
		return this.Id;	
	}

	//@requires Id >= 0;
	//@assignable this.Id;
	//@ensures this.Id == Id;
	public void setId(int Id) {
		this.Id = Id;
	}

	//@assignable this.tTag && this.profile;
	//@ensures this.tTag == typeTag && this.profile == profile;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Add(/*@ non_null@*/TTag typeTag,/*@ non_null@*/ Profile profile,/*@ non_null@*/ Transaction transaction) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >= 0;
	//@assignable this.tTag;
	//@ensures this.tTag == typeTag ;
	//@ensures \result == true;
	//@ signals_only UnsupportedOperationException;
	public/*@ pure @*/ boolean Update(int Id, /*@ non_null@*/TTag typeTag, /*@ non_null@*/Transaction transaction) {
		throw new UnsupportedOperationException();
	}

	//@requires Id >=0;
	//@ensures \result ==true;
	//@ signals_only UnsupportedOperationException;
	public /*@ pure @*/ boolean Delete(int Id) {
		throw new UnsupportedOperationException();
	}

}